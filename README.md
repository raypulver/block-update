# You want the new block?
## You're gonna get it

### Install

Get a working NodeJS installation, clone the repo, then in the project directory run

```shell
npm install
```

Then in the project directory run

```shell
npm start
```

You will be notified instantly of bitcoin block updates
